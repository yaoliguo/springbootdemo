package com.yao.springbootdemo.control;

import com.yao.springbootdemo.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;


@Controller
@RequestMapping("/hello")
public class Hello {

    @Autowired
    User user;

    @RequestMapping("/hello")
    @ResponseBody
    public Object hello(){



        return user;
    }
}
