package com.yao.webview.service;

import com.baomidou.mybatisplus.service.IService;
import com.yao.webview.dao.entity.User;

public interface IUserService extends IService<User>{
}
