package com.yao.webview.service.impl;

import com.yao.webview.dao.UserDao;
import com.yao.webview.dao.entity.User;
import com.yao.webview.service.IUserService;
import com.yao.webview.service.base.BaseServiceImpl;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl extends BaseServiceImpl<UserDao,User> implements IUserService {

}
