package com.yao.webview.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.yao.webview.dao.entity.User;
import org.springframework.stereotype.Repository;

@Repository
public interface UserDao extends BaseMapper<User> {

}
