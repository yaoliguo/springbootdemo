package com.yao.webview.dao.entity;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;

import java.io.Serializable;
import java.util.Date;

@TableName(value = "employee")
public class User extends Model<User> implements Serializable{

    @TableId(value = "id",type = IdType.AUTO)
    private int id;

    private String name;

    private int sex;

    private Date birthday;

    @TableField(value = "contact_code")
    private String contactCode;

    private int del;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getSex() {
        return sex;
    }

    public void setSex(int sex) {
        this.sex = sex;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public String getContactCode() {
        return contactCode;
    }

    public void setContactCode(String contactCode) {
        this.contactCode = contactCode;
    }

    public int getDel() {
        return del;
    }

    public void setDel(int del) {
        this.del = del;
    }


    @Override
    protected Serializable pkVal() {

        return id;
    }
}
