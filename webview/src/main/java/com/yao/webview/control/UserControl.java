package com.yao.webview.control;

import com.yao.webview.dao.UserDao;
import com.yao.webview.dao.entity.User;
import com.yao.webview.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping(value = "Uesr")
public class UserControl {

    @Autowired
    //IUserService userService;
    UserDao userDao;

    @ResponseBody
    @RequestMapping("/userList")
    public Object getUesrList(){

        User u = new User();
        u.setName("张三");

        return u;
    }

}
